# Starter Web Project

This is a starter project for me to learn GIT

## Introduction

This example is showing how the different parts of the GIT repository work.

## Purpose

As above, to learn GIT. Rebasing example is working great! This is amazing.

## Deployment

This can be put on any web server.

## How to Contribute

please for this repo and then issue pull requests for review.

### Copyright

2019 Git training All rights Absorbed